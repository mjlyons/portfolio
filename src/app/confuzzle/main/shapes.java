package app.confuzzle.main;

public class shapes {
	public coor c[];
	public int size;
	
	private int rnd(double d) {
		if (d<0.0) 
			return (int)(d-0.5);
		else 
			return (int)(d+0.5);
	}
	
	public shapes(int n, int s) {
		size=s;
		int i;
	    c = new coor[n];
	    for(i=0;i<n;i++)
	    	c[i] = new coor(0,0);
	    
		double dtheta = 2*Math.PI/(double)n, theta=0.0;
		
		for(i=0;i<n;i++) {
			c[i].setX(rnd(size*Math.cos(theta))+100);
			c[i].setY(rnd(size*Math.sin(theta))+100);
			theta+=dtheta;
		}
	}
}
