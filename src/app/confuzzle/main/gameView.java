package app.confuzzle.main;

import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

class gameView extends SurfaceView implements SurfaceHolder.Callback {
	private gameThread thread;
	
	public int mTouchX, mTouchY;
	public int score,hscore,diff;
	public long mTime,mLasttime;
 
	public int x1,y1,x2,y2;
	
	public boolean gameOver;
	
	public String scoreText,hScoreText, text, text2, gOver, replay, mMenu;
	public Paint tPaint;
	public Paint sPaint;
	public ShapeDrawable shape[];
	public int num[],colour[],theta[];
	public Random rand;
	
	private MediaPlayer rightMP, wrongMP;
	
	public SharedPreferences sp,settings;
	public Resources res;
	
	public gameView(Context context) {
		super(context);
		sp = context.getSharedPreferences("My_Prefs", Context.MODE_PRIVATE);
		settings = PreferenceManager.getDefaultSharedPreferences(context);
		String s = settings.getString("pref_difficulty","0");
		diff = Integer.parseInt(s);
		res = context.getResources();
		getHolder().addCallback(this);
		
		gameOver = false;
		
		scoreText=res.getString(R.string.score);
		hScoreText=res.getString(R.string.hscore);
		
		tPaint = new Paint();
		tPaint.setColor(0xFFFFFFFF);
		tPaint.setTextSize(30);
		sPaint = new Paint();
		sPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		
		shape = new ShapeDrawable[4];
		num = new int[4];
		colour = new int[4];
		theta= new int[4];
														
		rand = new Random();
		
		x1 = getWidth()/4;
		x2 = x1*3;
	    
	    y1 = getHeight()/4;
	    y2 = y1*3;
		
	    rightMP = MediaPlayer.create(context, R.raw.right);
	    wrongMP = MediaPlayer.create(context, R.raw.wrong);
	    
//		setHighscore(0);
		hscore=getHighscore();
		score=0;
		text = hScoreText + Integer.toString(hscore);
		text2 = scoreText + Integer.toString(score);
		
		mTouchX=0;
		mTouchY=0;
	}

	@Override
	public void onDraw(Canvas canvas) {
		canvas.drawColor(0xFF000000);
		
		if (!gameOver) {
			canvas.save();
			canvas.rotate( (float) theta[0],getWidth()/4,getHeight()/4);
			shape[0].setBounds(getWidth()/4 - 100, getHeight()/4 - 100, getWidth()/4 + 100, getHeight()/4 + 100);
			shape[0].draw(canvas);
			canvas.restore();

			canvas.save();
			canvas.rotate( (float) theta[1],getWidth()*3/4,getHeight()/4);
			shape[1].setBounds(getWidth()*3/4 - 100, getHeight()/4 - 100, getWidth()*3/4 + 100, getHeight()/4 + 100);
			shape[1].draw(canvas);
			canvas.restore();

			canvas.save();
			canvas.rotate( (float) theta[2],getWidth()/4,getHeight()*3/4);
			shape[2].setBounds(getWidth()/4 - 100, getHeight()*3/4 - 100, getWidth()/4 + 100, getHeight()*3/4 + 100);
			shape[2].draw(canvas);
			canvas.restore();

			canvas.save();
			canvas.rotate( (float) theta[3],getWidth()*3/4,getHeight()*3/4);
			shape[3].setBounds(getWidth()*3/4 - 100, getHeight()*3/4 - 100, getWidth()*3/4 + 100, getHeight()*3/4 + 100);
			shape[3].draw(canvas);
			canvas.restore();

			canvas.drawText(text, 5, 35, tPaint);
			canvas.drawText(text2, 5, 65, tPaint);
		}
		else {
			
		}
	}
	
	@Override
    public boolean onTouchEvent(MotionEvent event) {
		mTouchX = (int)event.getX();
		mTouchY = (int)event.getY();
		if (event.getAction() == MotionEvent.ACTION_UP) {
			if (biggestNum() == whatShape(mTouchX, mTouchY)) {
				rightMP.start();
				score +=1;
				mLasttime = System.currentTimeMillis();
				
			}
			else {
				wrongMP.start();
				score=0;
			}
			generateNewShapes();
			
		}
		return true;
    }
	
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		x1 = getWidth()/4;
		x2 = x1*3;
	    
	    y1 = getHeight()/4;
	    y2 = y1*3;
	}

	public void surfaceCreated(SurfaceHolder holder) {
		setWillNotDraw(false);
		thread = new gameThread(getHolder(),this);
		thread.setRunning(true);
		thread.start();
		generateNewShapes();
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		try {
			thread.setRunning(false);
			thread.join();
		} catch (InterruptedException e) {}
	}

	public void setHighscore(int n) {
		Editor editor = sp.edit();
		editor.putInt("cHScore", n);
		editor.commit();
	}

	public int getHighscore() {
		int s = sp.getInt("cHScore", 0); //0 is the default value
		return s;
	}

	public ShapeDrawable createShape(int n, int size, int color) {
		ShapeDrawable sd;
		Path p = new Path();
		shapes s = new shapes(n,size);
		p.moveTo(s.c[0].x, s.c[0].y);
		for(int i=1;i<n;i++){
			p.lineTo(s.c[i].x, s.c[i].y);
		}
		p.lineTo(s.c[0].x, s.c[0].y);
		PathShape ps = new PathShape(p,size*2,size*2);
		sd = new ShapeDrawable(ps);
		sPaint.setColor(color);
		sd.getPaint().set(sPaint);
		return sd;
	}
	
	public int random(int n) {
		int r = rand.nextInt(n);
		int s = rand.nextInt(n);
		int t = rand.nextInt(n);
		if (r<3 || s<3 || t<3) return 3;
		if (r<=s && r<=t) 
			return r;
		if (s<=r && s<=t)
			return s;
		if (t<=r && t<=s)
			return t;
		return r;
	}
	
	public int toColor(int n) {
		switch (n) {
		case 1: return res.getColor(R.color.Green);
		case 2: return res.getColor(R.color.Blue);
		case 3: return res.getColor(R.color.Red);
		case 4: return res.getColor(R.color.Yellow);
		case 5: return res.getColor(R.color.Purple);
		case 6: return res.getColor(R.color.Pink);
		case 7: return res.getColor(R.color.Brown);
		case 8: return res.getColor(R.color.LBlue);
		case 9: return res.getColor(R.color.Orange);
		case 10: return res.getColor(R.color.White);
		case 11: return res.getColor(R.color.LBrown);
		case 12: return res.getColor(R.color.LPurple);
		case 13: return res.getColor(R.color.LGreen);
		case 14: return res.getColor(R.color.Aqua);
		case 15: return res.getColor(R.color.DBlue);
		case 16: return res.getColor(R.color.DRed);
		case 17: return res.getColor(R.color.DYellow);
		case 18: return res.getColor(R.color.DGreen);
		case 19: return res.getColor(R.color.DGrey);
		case 20: return res.getColor(R.color.Grey);
		default: return res.getColor(R.color.White);
		}
	}
	
	public void randomColors() {
		int count=0;
		boolean done = false, ok = false;
		while (!done) {
			int a = rand.nextInt(20);
			if (a<1) a=1;
			for(int i=0;i<4;i++)
				if (a==colour[i]) ok = true;
			if (!ok) {
				colour[count]=a;
				count++;
				if (count==4) done = true;
			}
			ok = false;
		}
		for (int i = 0; i<4; i++)
			colour[i] = toColor(colour[i]);
	}
	
	public void generateNewShapes() {
		int n = 10+diff*5;
		boolean done = false,ok = false;
		int count=0;
		randomColors();
		
		while (!done) {
			int a = random(n);
			for(int i=0;i<4;i++)
				if (a==num[i]) ok = true;
			if (!ok) {
				num[count]=a;
				count++;
				if (count==4) done = true;
			}
			ok = false;
		}
		
		for(int i=0;i<4;i++) {
			shape[i] = createShape(num[i], 100, colour[i]);
			theta[i] = rand.nextInt(360);
		}
	}
	
	public int whatShape(int x, int y) {
		boolean left = false,up = false;
		if (x>x1-100 && x<x1+100) left = true;
		if (y>y1-100 && y<y1+100) up = true;
		if (up && left) return 0;
		if (up && !left) return 1;
		if (!up && left) return 2;
		if (!up && !left) return 3;
		return 0;
	}
	
	public int biggestNum() {
		int bn = num[0],ind= 0;
		for(int i=0;i<4;i++) 
			if (num[i] > bn) {
				bn = num[i];
				ind = i;
			}
		return ind;
	}
	
	public void GameOverDialog() {
		gameOver = true;
	}
	
	class gameThread extends Thread {
		public SurfaceHolder mSHolder;
		public gameView mgameView;
		private boolean running;
		
		public gameThread(SurfaceHolder holder, gameView gView) {
			mSHolder = holder;
			mgameView = gView;
		}
		
		public void setRunning(boolean b) {
			running = b;
		}
		
		public void run() {
			Canvas c;
			while (running) {
				c = null;
				try {
					c = mSHolder.lockCanvas(null);
					synchronized (mSHolder) {
						update();
						postInvalidate();
					}
				} finally {
					if (c!=null) mSHolder.unlockCanvasAndPost(c);
				}
			}
		}	
		
		public void update() {
			text2 = scoreText + Integer.toString(score);
			if (score>hscore) {
				hscore=score;
				text = hScoreText + Integer.toString(score);
				setHighscore(hscore);
			}
			if (mLasttime + 3000 - 500*diff < System.currentTimeMillis() && score!=0) {
				wrongMP.start();
				score = 0;
				generateNewShapes();
				mLasttime = System.currentTimeMillis();  
			}
		}
	}
		
}