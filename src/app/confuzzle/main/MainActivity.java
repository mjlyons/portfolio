package app.confuzzle.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import app.confuzzle.main.gameView;

public class MainActivity extends Activity implements OnClickListener {
	public static final int DIFF_DIALOG = 1;
	
	public int mDiff;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
//    	mDiff = 0;
//    	TextView tv = (TextView) findViewById(R.id.difficulty);
//    	if (mDiff == 0)
//    		tv.setText(R.string.easy);
//    	if (mDiff == 1)
//    		tv.setText(R.string.med);
//    	if (mDiff == 2)
//    		tv.setText(R.string.hard);
    	setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
   
    
    public void onClick(View view) {
	}
    
    public void gotogames(View view){
    	Intent myIntent = new Intent(view.getContext(), GameMenu.class);
        view.getContext().startActivity(myIntent);
    }
    
    public void gotosettings(View view) {
    	Intent myIntent = new Intent(view.getContext(), SettingsActivity.class);
        view.getContext().startActivity(myIntent);
    }

    

    
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		
	}

	
		
	
}
