package app.confuzzle.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class GameMenu extends Activity implements OnClickListener{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_game_menu, menu);
        return true;
    }

	public void onClick(View view) {
		
	}

	public void classic(View view) {
		Intent myIntent = new Intent(view.getContext(), ClassicActivity.class);
        view.getContext().startActivity(myIntent);
	}

	public void timeattack(View view) {
//		Intent myIntent = new Intent(view.getContext(), TimeAttackActivity.class);
//        view.getContext().startActivity(myIntent);
	}

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

}
