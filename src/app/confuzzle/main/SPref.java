package app.confuzzle.main;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;

public class SPref extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spref);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_spref, menu);
        return true;
    }
    
    public void setHighscore(int n) {
		SharedPreferences prefs = getSharedPreferences("My_Prefs", Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putInt("cHScore", n);
		editor.commit();
	}
    
    public int getHighscore() {
		SharedPreferences prefs = getSharedPreferences("My_Prefs", Context.MODE_PRIVATE);
		int score = prefs.getInt("cHScore", 0); //0 is the default value
		return score;
    }	
}
