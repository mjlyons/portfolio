package app.confuzzle.main;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import app.confuzzle.main.gameView;

public class ClassicActivity extends Activity {
	private gameView gView;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gView = new gameView(this);
        setContentView(gView);
    }
	
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_classic, menu);
        return true;
    }

}
